import React from 'react';

const ImageCard = (props) => {
  const { post_description, post_image_url } = props;
  return (
    <div className="row">
      <div className="col-md-12 px-0">
        <div className="rounded-lg overflow-hidden">
          <img src={post_image_url} className="img-fluid " alt={post_description}></img>
        </div>
      </div>
    </div>
  );
};

export default ImageCard;
