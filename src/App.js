import React, { useEffect, useState } from 'react';
import WallFetchImagesAPI from './apis/WallFetchImages';
import ImageCard from './components/ImageCard';
import Masonry from 'react-masonry-css';
import InfiniteScroll from 'react-infinite-scroll-component';
import './App.css'
let pageNum = 1;

const App = () => {
  // const [layoutReady, setLayoutReady] = useState(false)
  const [imagesArray, setImagesArray] = useState([]);
  const [totalPages, setTotalPages] = useState(0);

  
  const fetchImages = (pageNumber) => {
    WallFetchImagesAPI.get('/', { params: { page: pageNumber } })
      .then((res) => {
        console.log(res.data)
        setImagesArray([...imagesArray, ...res.data.feed]);
        setTotalPages(res.data.totalPages);
      })
      .catch((err) => console.log(err));
  };

  // function to fetch images based on the
  useEffect(() => {
    fetchImages(pageNum);
  }, []);

  

  const breakpointColumnsObj = {
    default: 4,
    1200: 3,
    992: 3,
    768: 2,
    576: 1,
  };

  return (
    <div className="App">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <InfiniteScroll
              dataLength = {imagesArray.length}
              pageStart={0}
              next={() => fetchImages(++pageNum)}
              loadMore={() => fetchImages(++pageNum)}
              hasMore={pageNum < totalPages ? true : false}
              loader={ 
                <h4>Loading...</h4>
            }
            >
              <Masonry
                breakpointCols={breakpointColumnsObj}
                className="masonry-grid"
                columnClassName="masonry-grid_column"
              >
                {imagesArray.map((image) => (
                  <ImageCard {...image} />
                ))}
              </Masonry>
            </InfiniteScroll>
          </div>
        </div>
      </div>
    </div>
  );

  
}

export default App;
