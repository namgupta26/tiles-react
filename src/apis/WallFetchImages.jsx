import axios from 'axios';

const WallFetchImagesAPI = axios.create({
  baseURL: 'http://127.0.0.1:5100/feed/wall',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  params: {
    user: 'User29'
  },
});

export default WallFetchImagesAPI;
