# Tiles - Social Media Application #

Tiles is an imaginary social media application where all you see is a pretty wall of photos uploaded by your friends but for the sake of this assignment, you need to make a submission on only one of the two key pages here - Home or Profile. Home shows you popular photos from everyone in your friends list while Profile shows photos only from one particular user at a time. Feel free to use your discretion on further details like shape and size of these tiles, the border around them, how many should we put per line, responsive design, basic UX principles, the ranking algorithm etc etc

### What is included this repository for? ###

* Frontend Implementation - React

## Installation ##

Use npm to install dependencies

```bash
npm install
```

Run the backend using

```bash
npm start
```

## Configuration ##

* Backend URL - change baseURL present in /src/apis/WallFetchImages.jsx to modify.

## Who do I talk to? ##

* Naman Gupta - namgupta26@gmail.com